var express = require('express');
var request = require('request');
var router = express.Router();

/* GET users listing. */
router.get('/2019-12-01', function (req, res, next) {

  request("https://apirecruit-gjvkhl2c6a-uc.a.run.app/compras/2019-12-01", (err, response, body) => {
    if (!err) {
      const resumen = JSON.parse(body);
      const total = resumen.map((item, index) => item.monto)
      const totalmonto = total.map((item, index) => typeof item !== "undefined" ? item : 0)
      const tdc = resumen.filter((item, index) => typeof item.tdc !== "undefined")
      const tdcmonto = tdc.map((item, index) => item.monto)
      const tdcname = tdc.map((item, index) => item.tdc)

      const test = {}

      tdc.map(tdc =>
        test[tdc.tdc] = tdc.monto
      );


      const users = [{
        "total": (totalmonto.reduce((a, b) => a + b, 0) * 5).toFixed(2),
        "comprasPorTDC": test,
        "nocompraron": 100,
        "compraMasAlta": 500
      }
      ];
      // res.send(resumen.map(item => item.clientId));
      res.send(users);
      // res.send(resumen);
      // console.log(total);
    }
  })
});
module.exports = router;
